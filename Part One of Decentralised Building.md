# Collaborative Decentralised Design and Development

## Part one

These are our notes and records of who built what from around June-July 2019.

### To Build:

Please claim a section to work on, and edit this list as pages are completed as you go!

**Site Map**

* [x] Main home page setup and svelte sections ~ mnin

* [x] Splash ~ mnin
    * [x] download page ~ mnin
* [x] Values: Quickly mentions 4 values, Button “read more” which takes us to another page that explains all these ~ mnin
    * [x] Diversity/intersectionality ~ mnin
    * [x] Local-first ~ mnin
    * [x] Offline usable ~ mnin
    * [x] Open sourced ~ mnin
    * [x] Link to the SSB Principles Stack
* [x] Decentralised mini explainer banner ~ corlock
    * [c] Short explanation of decentralisation
    * [x] Link to the Protocol Guide ~ cel
    * [x] Link to Research & Publications ~ cel
        * [x] Research & Publications Page ~ cel
* [x] Proof we legit
    * [x] Videos ~ polylith
    * [x] Fun quote ~ endo
* [x] For contributors ~ 🌔 evening crew (polylith)
* [x] Mapping the ecosystem ~ 🌔 evening crew (endo)


### Decentralised Build Record

Take a note of approx. time you've spent on things. We plan to reimburse people through the [Open Collective](https://opencollective.com/access) :D

Right now some of this is in the airtable spreadsheet. If you don't have access to it or prefer to use keep record here, please do so!


| Section/Task                                                          | Whom                  | Date      | Time       |
|-----------------------------------------------------------------------|-----------------------|-----------|------------|
| Week One - Project Organising                                         | mnin, endo             | 10/6/19 | 3:00:00    |
| Week Two - Project Organising                                         | mnin                  | 17/6/19 | 1:00:00    |
| Design site map / General project setup                               | mnin, Angelica Blevins | 20/6/19 | 1:00:00    |
| Pair project set up                                                   | mnin, Angelica Blevins | 20/6/19 | 11:00      |
| Pair program with Angelica                                            | mnin, Angelica Blevins | 22/6/19 | 1:11:00    |
| Continue to break up site into codeable sections                      | mnin                  | 23/6/19 | 2:30:00    |
| Week Three - Project Organising                                       | mnin, endo             | 25/6/19 | 3:00:00    |
| admin/dev setup                                                       | endo                  | 27/6/19   | 1:09:00    |
| made Not Match / 404 page - including styles                          | mnin                  | 28/6/19 | 1:00:00    |
| Update to Page Not Found                                              | mnin                  | 29/6/19 | 15:00      |
| Post on Scuttle Butt update on things, and also push a little of code | mnin                  | 30/6/19 | 1:30:00    |
| Comms on Scuttle Butt                                                 | mnin                  | 2/7/19  | 1:00:00    |
| admin/comms                                                           | endo                  | 2/7/19    | 21:00      |
| Code and comms on SSB                                                 | mnin, endo             | 3/7/19  | 20:00      |
| Admin and comms on ssb                                                | mnin                  | 3/7/19    |   33:00 |
| Main Splash Page                       | mnin          | 3/7/19    |   60:00 |
| troubleshooting svelte                 | endo, polylith| 3/7/19    |   60:00 |
| documentation/admin                    | endo          | 3/7/19    |   40:00 |
| svelte troubleshooting                 | endo, polylith| 4/7/19	   | 1:00:00 |
| coded download page                    | mnin          | 4/7/19    |   60:00 |
| coded values section                   | mnin          | 4/7/19    |   35:00 |
| started code on proof we legit section | mnin          | 4/7/19    |   22:00 |
| admin stuff                            | endo          | 5/7/19    |   50:00 |
| call & admin                           | mnin, endo    | 6/7/19    |   45:00 |
| cleaning up build records              | mnin          | 6/7/19    |   45:00 |
| code & comms up to today               | polylith      | 6/9/19    |   8:47:00 |
| values - make a cute see more button and style clean up   | mnin          | 7/7/19    |   45:00 |
| admin/comms                            | endo          | 8/7/19    | 10:00 |
| pull merges and reviewing Accessibility CSS               | mnin, Sam     | 8/7/19    |   20:00 |
| Values page                                               | mnin          | 8/7/19    |   60:00 |
| coding + admin                                            | endo          | 9/7/19    | 3:02:00 |
| Coding Values page on a plane! -  working D&I             | mnin          | 9/7/19    |   60:00 |
| Values Page - making the link to our SSB Principles Stack | mnin          | 9/7/19    |   33:00 |
| Fixing styles and merge conflicts                         | mnin          | 11/7/19   | 2:16:51 |
| add decentralized minibanner, learning svelte             | corlock       | 16/7/19   | 2:00:00 |
| coding/repo management                                    | endo          | 16/7/19   | 1:15:00 |
| migrating to static version                               | cel, mnin, Angelica | 24/7/19 | 2:00:00 |
| migrating to static site                                  | cel           | 25/7/19 | 1:30:00 |
| moving research to separate page                          | cel, mnin     | 25/7/19 | 0:40:00 |
| dispersing funds comms | mnin, endo | 28/7/19 | 1:00:00 |
